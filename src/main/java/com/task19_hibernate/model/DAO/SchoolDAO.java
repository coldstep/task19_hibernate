package com.task19_hibernate.model.DAO;

import com.task19_hibernate.model.Entities.School;

public interface SchoolDAO extends GeneralDAO<School> {

}
