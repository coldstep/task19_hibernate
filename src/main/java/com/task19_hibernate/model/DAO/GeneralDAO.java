package com.task19_hibernate.model.DAO;

public interface GeneralDAO<Entity> {

  void selectAll();

  boolean insert(Entity entity);

  boolean update(Entity entity);

  boolean delete(Entity entity);


}
