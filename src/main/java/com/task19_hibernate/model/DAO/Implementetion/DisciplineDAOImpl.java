package com.task19_hibernate.model.DAO.Implementetion;

import com.task19_hibernate.ConnectionManager;
import com.task19_hibernate.model.DAO.DisciplineDAO;
import com.task19_hibernate.model.Entities.Discipline;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class DisciplineDAOImpl implements DisciplineDAO {


  @Override
  public void selectAll() {
    try (Session session = ConnectionManager.getSession()) {
      Query<Discipline> query = session.createQuery("from Discipline ");
      for (Object object : query.list()) {
        System.out.println(object.toString());
      }
    }
  }

  @Override
  public boolean insert(Discipline discipline) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.save(discipline);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean update(Discipline discipline) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.update(discipline);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean delete(Discipline discipline) {
    try(Session session = ConnectionManager.getSession()) {
    session.beginTransaction();
    session.delete(discipline);
    session.getTransaction().commit();
    return true;
  }catch (HibernateException ex){
    ex.printStackTrace();
    return false;
  }

  }
}
