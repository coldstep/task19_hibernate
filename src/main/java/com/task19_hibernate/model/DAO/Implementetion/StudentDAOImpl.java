package com.task19_hibernate.model.DAO.Implementetion;

import com.task19_hibernate.ConnectionManager;
import com.task19_hibernate.model.DAO.StudentDAO;
import com.task19_hibernate.model.Entities.Student;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class StudentDAOImpl implements StudentDAO {

  @Override
  public void selectAll() {
    try (Session session = ConnectionManager.getSession()) {
      Query<Student> query = session.createQuery("from Student");
      for (Object object : query.list()) {
        System.out.println(object.toString());
      }
    }

  }

  @Override
  public boolean insert(Student student) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.save(student);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean update(Student student) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.update(student);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean delete(Student student) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.delete(student);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }
}
