package com.task19_hibernate.model.DAO.Implementetion;

import com.task19_hibernate.ConnectionManager;
import com.task19_hibernate.model.DAO.CityDAO;
import com.task19_hibernate.model.Entities.City;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class CityDAOImpl implements CityDAO {


  @Override
  public void selectAll() {
    try (Session session = ConnectionManager.getSession()) {
      Query<City> query = session.createQuery("from City ");
      for (Object object : query.list()) {
        System.out.println(object.toString());
      }
    }
  }

  @Override
  public boolean insert(City city) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.save(city);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean update(City city) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.update(city);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean delete(City city) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.delete(city);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }
}
