package com.task19_hibernate.model.DAO.Implementetion;

import com.task19_hibernate.ConnectionManager;
import com.task19_hibernate.model.DAO.SchoolDAO;
import com.task19_hibernate.model.Entities.School;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class SchoolDAOImpl implements SchoolDAO {

  @Override
  public void selectAll() {
    try (Session session = ConnectionManager.getSession()) {
      Query<School> query = session.createQuery("from School ");
      for (Object object : query.list()) {
        System.out.println(object.toString());
      }
    }
  }

  @Override
  public boolean insert(School school) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.save(school);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean update(School school) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.update(school);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }

  @Override
  public boolean delete(School school) {
    try(Session session = ConnectionManager.getSession()) {
      session.beginTransaction();
      session.delete(school);
      session.getTransaction().commit();
      return true;
    }catch (HibernateException ex){
      ex.printStackTrace();
      return false;
    }
  }
}
