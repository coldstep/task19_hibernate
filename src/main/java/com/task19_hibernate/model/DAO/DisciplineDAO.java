package com.task19_hibernate.model.DAO;

import com.task19_hibernate.model.Entities.Discipline;

public interface DisciplineDAO extends GeneralDAO<Discipline> {

}
