package com.task19_hibernate.model.Entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class City {

  @Id
  @Column(name = "id")
  private long id;

  @Column( name = "name")
  private String name;

  public City() {
  }

  public City(long id) {
    this.id = id;
  }

  public City(String name) {
    this.name = name;
  }

  public City(long id, String name) {
    this.id = id;
    this.name = name;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "City{" +
        "id=" + id +
        ", name='" + name + '\'' +
        '}';
  }
}

