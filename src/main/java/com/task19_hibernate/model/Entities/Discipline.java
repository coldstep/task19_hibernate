package com.task19_hibernate.model.Entities;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;


@Entity
public class Discipline {

  @Id
  @Column(name = "id")
  private long id;


  @Column(name = "discipline_name")
  private String disciplineName;


  @ManyToMany
  @JoinTable(name = "student_has_discipline",
      joinColumns = @JoinColumn(name = "discipline_id", referencedColumnName = "id", nullable = false),
      inverseJoinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id", nullable = false))
  private List<Student> students;


  public Discipline() {
  }

  public Discipline(long id) {
    this.id = id;
  }

  public Discipline(String disciplineName) {
    this.disciplineName = disciplineName;
  }

  public Discipline(long id, String disciplineName) {
    this.id = id;
    this.disciplineName = disciplineName;
  }

  public Discipline(long id, String disciplineName,
      List<Student> students) {
    this.id = id;
    this.disciplineName = disciplineName;
    this.students = students;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getDisciplineName() {
    return disciplineName;
  }

  public List<Student> getStudents() {
    return students;
  }

  public void setStudents(List<Student> students) {
    this.students = students;
  }

  public void setDisciplineName(String disciplineName) {
    this.disciplineName = disciplineName;
  }

  @Override
  public String toString() {
    return "Discipline{" +
        "id=" + id +
        ", disciplineName='" + disciplineName + '\'' +
        '}';
  }
}
