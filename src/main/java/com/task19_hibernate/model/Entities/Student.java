package com.task19_hibernate.model.Entities;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

@Entity
public class Student {

  @Id
  @Column(name = "id")
  private long id;
  @Column(name = "surname")
  private String surname;
  @Column(name = "name")
  private String name;
  @Column(name = "age")
  private int age;
  @Column(name = "email")
  private String email;
  @ManyToOne
  @JoinColumn(name = "school_id", referencedColumnName = "id", nullable = false)
  private School school;
  @ManyToOne
  @JoinColumn(name = "city_id", referencedColumnName = "id", nullable = false)
  private City city;


  @ManyToMany(mappedBy = "students")
  private List<Discipline> disciplines;

  public Student() {
  }

  public Student(long id) {
    this.id = id;
  }

  public Student(long id, String surname, String name, int age, String email,
      School school, City city, String fullName) {
    this.id = id;
    this.surname = surname;
    this.name = name;
    this.age = age;
    this.email = email;
    this.school = school;
    this.city = city;
  }

  public Student(long id, String surname, String name, int age, String email,
      School school, City city, String fullName,
      List<Discipline> disciplines) {
    this.id = id;
    this.surname = surname;
    this.name = name;
    this.age = age;
    this.email = email;
    this.school = school;
    this.city = city;
    this.disciplines = disciplines;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public School getSchool() {
    return school;
  }

  public void setSchool(School school) {
    this.school = school;  }

  public City getCity() {
    return city;
  }

  public void setCity(City city) {
    this.city = city;
  }

  @Override
  public String toString() {
    return "Student{" +
        "id=" + id +
        ", surname='" + surname + '\'' +
        ", name='" + name + '\'' +
        ", age=" + age +
        ", email='" + email + '\'' +
        ", school=" + school +
        ", city=" + city +
        ", disciplines=" + disciplines +
        '}';
  }
}
