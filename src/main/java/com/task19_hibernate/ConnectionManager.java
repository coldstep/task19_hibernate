package com.task19_hibernate;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class ConnectionManager {

  private static SessionFactory ourSessionFactory;

  public ConnectionManager() {
    Configuration configuration = new Configuration();
    configuration.configure();
    ourSessionFactory = configuration.buildSessionFactory();
  }


  public static Session getSession() throws HibernateException {
    if (ourSessionFactory == null) {
      Configuration configuration = new Configuration();
      configuration.configure();
      ourSessionFactory = configuration.buildSessionFactory();
    }
    return ourSessionFactory.openSession();
  }

  public static void closeSessionFactory() {
    try {
      ourSessionFactory.close();
    } catch (HibernateException ex) {
      ex.printStackTrace();
    }

  }

}
