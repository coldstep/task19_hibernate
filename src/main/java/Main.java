import com.task19_hibernate.ConnectionManager;
import com.task19_hibernate.model.DAO.GeneralDAO;
import com.task19_hibernate.model.DAO.Implementetion.CityDAOImpl;
import com.task19_hibernate.model.DAO.Implementetion.SchoolDAOImpl;
import com.task19_hibernate.model.DAO.Implementetion.StudentDAOImpl;
import com.task19_hibernate.model.DAO.StudentDAO;
import com.task19_hibernate.model.Entities.City;
import com.task19_hibernate.model.Entities.School;
import com.task19_hibernate.model.Entities.Student;
import org.hibernate.query.Query;
import org.hibernate.Session;

public class Main {

  public static void main(final String[] args) throws Exception {

    final Session session = ConnectionManager.getSession();
    try {
//      System.out.println("querying all the managed entities...");
//      final Metamodel metamodel = session.getSessionFactory().getMetamodel();
//      for (EntityType<?> entityType : metamodel.getEntities()) {
//        final String entityName = entityType.getName();
//        final Query query = session.createQuery("from " + entityName);
//        System.out.println("executing: " + query.getQueryString());
//        for (Object o : query.list()) {
//          System.out.println("  " + o);
//        }
//      }
//    } finally {
//      session.close();
//      connectionManager.closeSessionFactory();
//    }
//    try {
//      GeneralDAO generalDAO = new StudentDAOImpl();
//      generalDAO.selectAll();
//
//      Student student = new Student();
//      student.setId(6);
//      student.setSurname("hitler");
//      student.setName("adolf");
//      student.setAge(123);
//      student.setEmail("mainkamf@gmail.com");
//      student.setCity(new City(1));
//      student.setSchool(new School(3));
////
//      new CityDAOImpl().insert(new City(6,"New York"));
//      new SchoolDAOImpl().insert(new School(6,"schoasol","aslfk","aofl"));
//      generalDAO.insert(student);
//generalDAO.delete(student);
//      generalDAO.selectAll();

//      School school = (School) session.createQuery("from School where name = 'School №44'").list().get(0);
//      if (school!= null){
//        System.out.println(school.toString());
//      }
//
//      StudentDAO studentDAO = new StudentDAOImpl();

    School school  = (School) session.load(School.class, 6L);
      System.out.println(school.toString());
      school.setName("School №23");
      school.setAddress("lipinskogo");
      school.setDirector("Petya");
      session.beginTransaction();
      session.update(school);
      session.getTransaction().commit();

    }finally {
      session.close();
      ConnectionManager.closeSessionFactory();
    }
  }
}